package bg.varna.fitness.TrainingRegimesShop.services;

import bg.varna.fitness.TrainingRegimesShop.base.TestBase;
import bg.varna.fitness.TrainingRegimesShop.domain.entities.User;
import bg.varna.fitness.TrainingRegimesShop.repositories.api.UserRepository;
import bg.varna.fitness.TrainingRegimesShop.services.api.UserService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

public class UserServiceTest extends TestBase
{
    @MockBean
    UserRepository userRepository;

    @Autowired
    UserService userService;

    private static final String username = "niki";

    @BeforeEach
    public void setupTest()
    {
        Mockito.when(userRepository.findByUsername(username))
                .thenReturn(new User("niki", "123"));
    }

    @Test
    void getUsernameOfUserByUsername()
    {

        User user = userRepository.findByUsername(username);
        Assert.assertEquals("niki", user.getUsername());
    }

    @Test
    void getPasswordOfUserByUsername()
    {

        User user = userRepository.findByUsername(username);
        Assert.assertEquals("123", user.getPassword());
    }
}
