package bg.varna.fitness.TrainingRegimesShop.models;


import bg.varna.fitness.TrainingRegimesShop.domain.entities.Image;

import java.util.ArrayList;
import java.util.List;


public class CoachViewModel
{
    private long id;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String address;
    private int experience;
    private Image image;

    private List<TrainingRegimeViewModel> regimes;

    public CoachViewModel()
    {

    }


    public CoachViewModel(long id, String firstName, String lastName, String phoneNumber, String address, int experience,
                          Image image, List<TrainingRegimeViewModel> regimes)
    {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.experience = experience;
        this.image = image;
        this.regimes = regimes;
    }


    public long getId()
    {
        return id;
    }


    public void setId(long id)
    {
        this.id = id;
    }


    public String getFirstName()
    {
        return firstName;
    }


    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }


    public String getLastName()
    {
        return lastName;
    }


    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }


    public String getPhoneNumber()
    {
        return phoneNumber;
    }


    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }


    public String getAddress()
    {
        return address;
    }


    public void setAddress(String address)
    {
        this.address = address;
    }


    public int getExperience()
    {
        return experience;
    }


    public void setExperience(int experience)
    {
        this.experience = experience;
    }


    public Image getImage()
    {
        return this.image;
    }


    public void setImage(Image image)
    {
        this.image = image;
    }


    public List<TrainingRegimeViewModel> getRegimes()
    {
        return new ArrayList<>(regimes);
    }


    public void setRegimes(List<TrainingRegimeViewModel> regimes)
    {
        this.regimes = new ArrayList<>(regimes);
    }

    public static class CoachDTOBuilder
    {
        private long id;
        private String firstName;
        private String lastName;
        private String phoneNumber;
        private String address;
        private int experience;
        private Image image;
        private List<TrainingRegimeViewModel> regimes;

        public CoachDTOBuilder setId(long id)
        {
            this.id = id;
            return this;
        }


        public CoachDTOBuilder setFirstName(String firstName)
        {
            this.firstName = firstName;
            return this;
        }


        public CoachDTOBuilder setLastName(String lastName)
        {
            this.lastName = lastName;
            return this;
        }


        public CoachDTOBuilder setPhoneNumber(String phoneNumber)
        {
            this.phoneNumber = phoneNumber;
            return this;
        }


        public CoachDTOBuilder setAddress(String address)
        {
            this.address = address;
            return this;
        }


        public CoachDTOBuilder setExperience(int experience)
        {
            this.experience = experience;
            return this;
        }


        public CoachDTOBuilder setImage(Image image)
        {
            this.image = image;
            return this;
        }


        public CoachDTOBuilder setRegimes(List<TrainingRegimeViewModel> regimes)
        {
            this.regimes = regimes;
            return this;
        }


        public CoachViewModel build()
        {
            return new CoachViewModel(this.id, this.firstName, this.lastName, this.phoneNumber, this.address, this.experience,
                    this.image, this.regimes);
        }
    }

}
