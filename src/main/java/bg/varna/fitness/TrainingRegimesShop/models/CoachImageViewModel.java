package bg.varna.fitness.TrainingRegimesShop.models;


import bg.varna.fitness.TrainingRegimesShop.domain.entities.Image;

public class CoachImageViewModel extends Image
{
    private long coachId;

    public CoachImageViewModel()
    {
    }


    public CoachImageViewModel(long id, String url, long coachId)
    {
        super(id, url);
        this.coachId = coachId;
    }


    public long getCoachId()
    {
        return coachId;
    }


    public void setCoachId(long coachId)
    {
        this.coachId = coachId;
    }

    public static class CoachImageDTOBuilder
    {
        private long id;
        private String url;
        private long coachId;

        public CoachImageDTOBuilder setId(long id)
        {
            this.id = id;
            return this;
        }

        public CoachImageDTOBuilder setCoachId(long coachId)
        {
            this.coachId = coachId;
            return this;
        }

        public CoachImageDTOBuilder setUrl(String url)
        {
            this.url = url;
            return this;
        }
        
        public CoachImageViewModel build()
        {
            return new CoachImageViewModel(this.id, this.url, this.coachId);
        }

    }
}
