package bg.varna.fitness.TrainingRegimesShop.models;


import java.math.BigDecimal;


public class TrainingRegimeViewModel
{
    private long id;

    private String regime;

    private BigDecimal price;

    private float rate;

    private TrainingRegimeTypeViewModel trainingRegimeTypeViewModel;

    private CoachViewModel coach;

    public TrainingRegimeViewModel()
    {

    }


    public TrainingRegimeViewModel(long id, String regime, BigDecimal price, float rate,
                                   TrainingRegimeTypeViewModel trainingRegimeTypeViewModel, CoachViewModel coach)
    {
        super();
        this.id = id;
        this.regime = regime;
        this.price = price;
        this.rate = rate;
        this.trainingRegimeTypeViewModel = trainingRegimeTypeViewModel;
        this.coach = coach;
    }


    public long getId()
    {
        return id;
    }


    public void setId(long id)
    {
        this.id = id;
    }


    public String getRegime()
    {
        return regime;
    }


    public void setRegime(String regime)
    {
        this.regime = regime;
    }


    public BigDecimal getPrice()
    {
        return price;
    }


    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }

    public float getRate()
    {
        return rate;
    }

    public void setRate(float rate)
    {
        this.rate = rate;
    }

    public TrainingRegimeTypeViewModel getTrainingRegimeTypeViewModel()
    {
        return trainingRegimeTypeViewModel;
    }

    public void setTrainingRegimeTypeViewModel(TrainingRegimeTypeViewModel trainingRegimeTypeViewModel)
    {
        this.trainingRegimeTypeViewModel = trainingRegimeTypeViewModel;
    }

    public CoachViewModel getCoach()
    {
        return coach;
    }

    public void setCoach(CoachViewModel coach)
    {
        this.coach = coach;
    }

    public static class TrainingRegimeDTOBuilder
    {
        private long id;
        private String regime;
        private BigDecimal price;
        private float rate;
        private TrainingRegimeTypeViewModel trainingRegimeType;
        private CoachViewModel coach;

        public TrainingRegimeDTOBuilder setId(long id)
        {
            this.id = id;
            return this;
        }

        public TrainingRegimeDTOBuilder setRegime(String regime)
        {
            this.regime = regime;
            return this;
        }

        public TrainingRegimeDTOBuilder setPrice(BigDecimal price)
        {
            this.price = price;
            return this;
        }

        public TrainingRegimeDTOBuilder setRate(float rate)
        {
            this.rate = rate;
            return this;
        }

        public TrainingRegimeDTOBuilder setCoachDTO(CoachViewModel coach)
        {
            this.coach = coach;
            return this;
        }

        public TrainingRegimeDTOBuilder setTrainingRegimeTypeDTO(TrainingRegimeTypeViewModel regimeType)
        {
            this.trainingRegimeType = regimeType;
            return this;
        }

        public TrainingRegimeViewModel build()
        {
            return new TrainingRegimeViewModel(id,regime, price, rate, trainingRegimeType, coach);
        }
    }

}
