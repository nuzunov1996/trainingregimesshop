package bg.varna.fitness.TrainingRegimesShop.models;

public class TrainingRegimeTypeViewModel
{
    private long id;
    private String type;
    
    public TrainingRegimeTypeViewModel()
    {
    
    }
    
    public TrainingRegimeTypeViewModel(long id, String type)
    {
        this.id = id;
        this.type = type;
    }
    
    public long getId()
    {
        return id;
    }
    public void setId(long id)
    {
        this.id = id;
    }
    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        this.type = type;
    }
    
    public static class TrainingRegimeTypeDTOBuilder
    {
        private long id;
        private String type;
        
        public TrainingRegimeTypeDTOBuilder setId(long id)
        {
            this.id = id;
            return this;
        }
        
        public TrainingRegimeTypeDTOBuilder setType(String type)
        {
            this.type = type;
            return this;                   
        }
        
        public TrainingRegimeTypeViewModel build()
        {
            return new TrainingRegimeTypeViewModel(id, type);
        }
    }
}
