package bg.varna.fitness.TrainingRegimesShop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainingRegimesShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainingRegimesShopApplication.class, args);
	}

}
