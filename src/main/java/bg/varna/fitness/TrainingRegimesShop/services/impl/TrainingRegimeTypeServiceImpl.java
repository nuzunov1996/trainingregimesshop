package bg.varna.fitness.TrainingRegimesShop.services.impl;

import bg.varna.fitness.TrainingRegimesShop.domain.entities.TrainingRegimeType;
import bg.varna.fitness.TrainingRegimesShop.repositories.api.TrainingRegimeTypeRepository;
import bg.varna.fitness.TrainingRegimesShop.services.api.BaseService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Nikolay Uzunov
 */

@Service
public class TrainingRegimeTypeServiceImpl implements BaseService<TrainingRegimeType>
{
    private static final Logger LOGGER = LogManager.getLogger(TrainingRegimeTypeServiceImpl.class);
    private TrainingRegimeTypeRepository trainingRegimeTypeRepository;

    @Override
    public List<TrainingRegimeType> get(TrainingRegimeType entity)
    {
        //Get by type
        if (entity.getType() != null)
        {
            return (List<TrainingRegimeType>) trainingRegimeTypeRepository.getByType(entity.getType());
        }
        return trainingRegimeTypeRepository.getAll();
    }

    @Override
    public void save(TrainingRegimeType entity)
    {
        trainingRegimeTypeRepository.save(entity);
    }

    @Override
    public void update(TrainingRegimeType entity)
    {
        trainingRegimeTypeRepository.update(entity);
    }

    @Override
    public void delete(TrainingRegimeType entity)
    {
        trainingRegimeTypeRepository.delete(entity);
    }

    @Override
    public void deactivate(TrainingRegimeType entity)
    {
        LOGGER.debug(entity);
        trainingRegimeTypeRepository.deactivate(entity);
    }
}
