package bg.varna.fitness.TrainingRegimesShop.services.impl;


import bg.varna.fitness.TrainingRegimesShop.domain.entities.Coach;
import bg.varna.fitness.TrainingRegimesShop.domain.entities.TrainingRegime;
import bg.varna.fitness.TrainingRegimesShop.models.CoachViewModel;
import bg.varna.fitness.TrainingRegimesShop.models.TrainingRegimeViewModel;
import bg.varna.fitness.TrainingRegimesShop.repositories.api.CoachRepository;
import bg.varna.fitness.TrainingRegimesShop.repositories.api.TrainingRegimeRepository;
import bg.varna.fitness.TrainingRegimesShop.services.api.BaseService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Nikolay Uzunov
 */

@Service
public class CoachServiceImpl implements BaseService<Coach>
{
    private static final Logger LOGGER = LogManager.getLogger(CoachServiceImpl.class);

    private static ModelMapper mapper;
    
    private CoachRepository coachRepository;

    private TrainingRegimeRepository regimeRepository;

    List<Coach> coaches = new ArrayList<>();

    public CoachServiceImpl(CoachRepository coachRepository, TrainingRegimeRepository trainingRegimeRepository)
    {
        this.coachRepository = coachRepository;
        this.regimeRepository = trainingRegimeRepository;
        mapper = new ModelMapper();
    }


    //TODO: FIX invocation of this setCoachRegimes.
    @Override
    public List<Coach> get(Coach entity)
    {
        LOGGER.debug(entity);
        // Get by name
        if (entity.getFirstName() != null && entity.getLastName() == null)
        {
            coaches = coachRepository.getByName(entity.getFirstName());
            return setCoachesRegimes();
        }
        // Get by last name
        if (entity.getLastName() != null && entity.getFirstName() == null)
        {
            coaches = coachRepository.getByLastName(entity.getLastName());
            return setCoachesRegimes();
        }
        // Get by full name
        if (entity.getFirstName() != null && entity.getLastName() != null)
        {
            coaches = coachRepository.getByFullName(entity.getFirstName(), entity.getLastName());
            return setCoachesRegimes();
        }
        return setCoachesRegimes();
    }


    @Override
    public void save(Coach entity)
    {
        LOGGER.debug(entity);
        coachRepository.save(entity);
    }


    @Override
    public void update(Coach entity)
    {
        LOGGER.debug(entity);
        coachRepository.update(entity);
    }


    @Override
    public void delete(Coach entity)
    {
        LOGGER.debug(entity);
        coachRepository.delete(entity);
    }


    @Override
    public void deactivate(Coach entity)
    {
        LOGGER.debug(entity);
        coachRepository.deactivate(entity);
    }


    private List<Coach> setCoachesRegimes()
    {
        coaches = coachRepository.getAll();
        List<CoachViewModel> coachesDTO = new ArrayList();
        List<TrainingRegime> regimes;
        List<TrainingRegimeViewModel> regimesDTO = new ArrayList<>();

        for (Coach coach : coaches)
        {
            CoachViewModel coachViewModel = mapper.map(coach, CoachViewModel.class);
            
            regimes = regimeRepository.getByCoachId(coach.getId());
            TrainingRegime regime = new TrainingRegime.TrainingRegimeBuilder()
                    .setRegime("<------REGIME1------>")
                    .build();
            regimes.add(regime);
            for (TrainingRegime trainingRegime : regimes)
            {
                regimesDTO.add(mapper.map(trainingRegime, TrainingRegimeViewModel.class));
            }
            coachViewModel.setRegimes(regimesDTO);
            coachesDTO.add(coachViewModel);
        }
        
        System.out.println(coachesDTO.get(0).getRegimes().get(0).getRegime());
        
        return coaches;
    }
}
