package bg.varna.fitness.TrainingRegimesShop.services.api;

import java.util.List;

/**
 * @author Nikolay Uzunov
 */

public interface BaseService<T>
{
    List<T> get(T entity);
    void save(T entity);
    void update(T entity);
    void delete(T entity);
    void deactivate(T entity);
}
