package bg.varna.fitness.TrainingRegimesShop.services.impl;

import bg.varna.fitness.TrainingRegimesShop.domain.entities.User;
import bg.varna.fitness.TrainingRegimesShop.models.RegisterViewModel;
import bg.varna.fitness.TrainingRegimesShop.repositories.api.UserRepository;
import bg.varna.fitness.TrainingRegimesShop.services.api.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService
{
    private static final Logger LOGGER = LogManager.getLogger(UserDetailsService.class);
    private final UserRepository userRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private static final ModelMapper mapper = new ModelMapper();

    public UserServiceImpl(UserRepository userRepository)
    {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
        try
        {
            User user = userRepository.findByUsername(username);
            return user;
        }
        catch (UsernameNotFoundException e)
        {
            LOGGER.error(e.getMessage());
            throw new UsernameNotFoundException(e.getMessage());
        }
    }

    @Override
    public void register(User user)
    {
        userRepository.insert(user);
    }

/*    @Override
    public void register(RegisterViewModel registerViewModel)
    {
        User user = mapper.map(registerViewModel, User.class);
        //String encodedPassword = encodePassword(registerViewModel.getPassword());
        //user.setPassword(encodedPassword);
        userRepository.insert(user);
    }*/

    private String encodePassword(String plainPassword)
    {
        String encodedPassword = bCryptPasswordEncoder.encode(plainPassword);
        return encodedPassword;
    }
}
