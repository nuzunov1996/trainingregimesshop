package bg.varna.fitness.TrainingRegimesShop.services.api;


import bg.varna.fitness.TrainingRegimesShop.domain.entities.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService
{
    void register(User user);
}
