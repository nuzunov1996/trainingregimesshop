package bg.varna.fitness.TrainingRegimesShop.services.impl;

import bg.varna.fitness.TrainingRegimesShop.domain.entities.TrainingRegime;
import bg.varna.fitness.TrainingRegimesShop.repositories.api.CoachRepository;
import bg.varna.fitness.TrainingRegimesShop.repositories.api.TrainingRegimeRepository;
import bg.varna.fitness.TrainingRegimesShop.services.api.BaseService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nikolay Uzunov
 */

@Service
public class TrainingRegimeServiceImpl implements BaseService<TrainingRegime>
{
    private static final Logger LOGGER = LogManager.getLogger(TrainingRegimeServiceImpl.class);
    private List<TrainingRegime> trainingRegimes = new ArrayList<>();
    private CoachRepository coachRepository;
    private TrainingRegimeRepository trainingRegimeRepository;

    public TrainingRegimeServiceImpl(TrainingRegimeRepository trainingRegimeRepository, CoachRepository coachRepository)
    {
        this.coachRepository = coachRepository;
        this.trainingRegimeRepository = trainingRegimeRepository;
    }

    @Override
    public List<TrainingRegime> get(TrainingRegime entity)
    {
        LOGGER.debug(entity);
        //SINGLE FILTERING
        //Get by coach id.
        if (entity.getCoachId() != 0 && entity.getRate() == 0
                && entity.getTrainingRegimeTypeId() == 0)
        {
            trainingRegimes = trainingRegimeRepository.getByCoachId(entity.getCoachId());
            return trainingRegimes;
        }
        //Get by rate.
        if (entity.getRate() != 0 && entity.getTrainingRegimeTypeId() == 0
                && entity.getCoachId() == 0)
        {
            trainingRegimes = trainingRegimeRepository.getByRate(entity.getRate());
            return trainingRegimes;
        }
        //Get by training regime type id.
        if (entity.getTrainingRegimeTypeId() != 0 && entity.getRate() == 0
                && entity.getCoachId() == 0)
        {
            trainingRegimes = trainingRegimeRepository.getByTrainingRegimeTypeId(
                    entity.getTrainingRegimeTypeId());
            return trainingRegimes;
        }
        return trainingRegimeRepository.getAll();
    }

    @Override
    public void save(TrainingRegime entity)
    {
        LOGGER.debug(entity);
        trainingRegimeRepository.save(entity);
    }

    @Override
    public void update(TrainingRegime entity)
    {
        LOGGER.debug(entity);
        trainingRegimeRepository.update(entity);
    }

    @Override
    public void delete(TrainingRegime entity)
    {
        LOGGER.debug(entity);
        trainingRegimeRepository.delete(entity);
    }

    @Override
    public void deactivate(TrainingRegime entity)
    {
        LOGGER.debug(entity);
        trainingRegimeRepository.deactivate(entity);
    }
    
    public List<TrainingRegime> setCoach()
    {
        trainingRegimes = trainingRegimeRepository.getAll();
        for (TrainingRegime trainingRegime : trainingRegimes)
        {
           // trainingRegime.setCoach(coachRepository.getById(trainingRegime.getCoachId()));
        }
        
        return trainingRegimes;
    }
}
