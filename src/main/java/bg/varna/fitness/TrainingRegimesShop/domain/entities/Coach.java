package bg.varna.fitness.TrainingRegimesShop.domain.entities;


import java.util.List;


public class Coach
{
    private long id;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String address;
    private int experience;

    public Coach()
    {

    }

    public Coach(long id, String firstName, String lastName, String phoneNumber, String address, int experience)
    {
        super();
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.experience = experience;
    }


    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public int getExperience()
    {
        return experience;
    }

    public void setExperience(int experience)
    {
        this.experience = experience;
    }

    @Override
    public String toString()
    {
        return "Coach{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address='" + address + '\'' +
                ", experience=" + experience +
                '}';
    }

    public static class CoachBuilder
    {
        private long id;
        private String firstName;
        private String lastName;
        private String phoneNumber;
        private String address;
        private int experience;

        public CoachBuilder setId(int id)
        {
            this.id = id;
            return this;
        }

        public CoachBuilder setFirstName(String firstName)
        {
            this.firstName = firstName;
            return this;
        }

        public CoachBuilder setLastName(String lastName)
        {
            this.lastName = lastName;
            return this;
        }

        public CoachBuilder setPhoneNumber(String phoneNumber)
        {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public CoachBuilder setAddress(String address)
        {
            this.address = address;
            return this;
        }

        public CoachBuilder setExperience(int experience)
        {
            this.experience = experience;
            return this;
        }

        public Coach build()
        {
            return new Coach(id, firstName, lastName, phoneNumber, address, experience);
        }
    }
}