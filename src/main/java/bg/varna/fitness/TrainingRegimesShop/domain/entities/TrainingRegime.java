package bg.varna.fitness.TrainingRegimesShop.domain.entities;

import java.math.BigDecimal;

public class TrainingRegime
{
    private long id;
    private String regime;
    private BigDecimal price;
    private float rate;
    private long coachId;
    private long trainingRegimeTypeId;

    public TrainingRegime()
    {

    }

    public TrainingRegime(long id, String regime, BigDecimal price, float rate, long coachId, long trainingRegimeTypeId)
    {
        super();
        this.id = id;
        this.regime = regime;
        this.price = price;
        this.rate = rate;
        this.coachId = coachId;
        this.trainingRegimeTypeId = trainingRegimeTypeId;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getRegime()
    {
        return regime;
    }

    public void setRegime(String regime)
    {
        this.regime = regime;
    }

    public BigDecimal getPrice()
    {
        return price;
    }

    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }

    public float getRate()
    {
        return rate;
    }

    public void setRate(float rate)
    {
        this.rate = rate;
    }

    public long getTrainingRegimeTypeId()
    {
        return trainingRegimeTypeId;
    }

    public long getCoachId()
    {
        return coachId;
    }

    public void setCoachId(long coachId)
    {
        this.coachId = coachId;
    }

    public void setTrainingRegimeTypeId(long trainingRegimeTypeId)
    {
        this.trainingRegimeTypeId = trainingRegimeTypeId;
    }

    public static class TrainingRegimeBuilder
    {
        private long id;
        private String regime;
        private BigDecimal price;
        private float rate;
        private long coachId;
        private long trainingRegimeTypeId;

        public TrainingRegimeBuilder setCoachId(long coachId)
        {
            this.coachId = coachId;
            return this;
        }

        public TrainingRegimeBuilder setId(long id)
        {
            this.id = id;
            return this;
        }

        public TrainingRegimeBuilder setRegime(String regime)
        {
            this.regime = regime;
            return this;
        }

        public TrainingRegimeBuilder setPride(BigDecimal price)
        {
            this.price = price;
            return this;
        }

        public TrainingRegimeBuilder setRate(float rate)
        {
            this.rate = rate;
            return this;
        }

        public TrainingRegimeBuilder setTrainingRegimeTypeId(long id)
        {
            this.trainingRegimeTypeId = trainingRegimeTypeId;
            return this;
        }

        public TrainingRegime build()
        {
            return new TrainingRegime(id, regime, price, rate, coachId, trainingRegimeTypeId);
        }
    }
}
