package bg.varna.fitness.TrainingRegimesShop.domain.entities;


public class TrainingRegimeType
{
    private long id;
    private String type;

    public TrainingRegimeType()
    {

    }

    public TrainingRegimeType(long id, String type)
    {
        super();
        this.id = id;
        this.type = type;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public static class TrainingRegimeTypeBuilder
    {
        private long id;
        private String type;

        public TrainingRegimeTypeBuilder setId(long id)
        {
            this.id = id;
            return this;
        }

        public TrainingRegimeTypeBuilder setType(String type)
        {
            this.type = type;
            return this;
        }

        public TrainingRegimeType build()
        {
            return new TrainingRegimeType(id, type);
        }
    }
}
