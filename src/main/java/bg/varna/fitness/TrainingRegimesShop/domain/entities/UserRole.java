package bg.varna.fitness.TrainingRegimesShop.domain.entities;

import org.springframework.security.core.GrantedAuthority;

public class UserRole implements GrantedAuthority
{
    private String role;

    public UserRole()
    {
    }

    public String getRole()
    {
        return role;
    }

    public void setRole(String role)
    {
        this.role = role;
    }

    @Override
    public String getAuthority()
    {
        return this.getRole();
    }
}
