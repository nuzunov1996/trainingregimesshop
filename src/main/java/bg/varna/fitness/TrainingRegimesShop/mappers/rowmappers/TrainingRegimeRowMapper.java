package bg.varna.fitness.TrainingRegimesShop.mappers.rowmappers;

import bg.varna.fitness.TrainingRegimesShop.domain.entities.TrainingRegime;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Nikolay Uzunov
 */

@Component
public class TrainingRegimeRowMapper implements RowMapper<TrainingRegime>
{
    @Override
    public TrainingRegime mapRow(ResultSet resultSet, int i) throws SQLException
    {
        TrainingRegime trainingRegime = new TrainingRegime();

        trainingRegime.setId(resultSet.getLong("id"));
        trainingRegime.setPrice(resultSet.getBigDecimal("price"));
        trainingRegime.setRate(resultSet.getFloat("rate"));
        trainingRegime.setTrainingRegimeTypeId(resultSet.getLong("training_regime_type_id"));
        trainingRegime.setCoachId(resultSet.getLong("coach_id"));
        trainingRegime.setRegime(resultSet.getString("regime"));

        return trainingRegime;
    }
}
