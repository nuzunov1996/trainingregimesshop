package bg.varna.fitness.TrainingRegimesShop.mappers.modelmapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

/**
 * @author Nikolay Uzunov
 */

@Component
public class Mapper<T>
{
    private ModelMapper mapper;

    public Mapper(ModelMapper mapper)
    {
        this.mapper = mapper;
    }

    public T map(Object filter, Class<T> clazz)
    {
        T object = mapper.map(filter, clazz);
        return object;
    }
}
