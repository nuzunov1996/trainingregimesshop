package bg.varna.fitness.TrainingRegimesShop.mappers.rowmappers;

import bg.varna.fitness.TrainingRegimesShop.domain.entities.Coach;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Nikolay Uzunov
 */

@Component
public class CoachRowMapper implements RowMapper<Coach>
{
    //check will this work if pass field which is not exists here!!!
    @Override
    public Coach mapRow(ResultSet resultSet, int i) throws SQLException
    {
        Coach coach = new Coach();

        coach.setId(resultSet.getLong("id"));
        coach.setFirstName(resultSet.getString("first_name"));
        coach.setLastName(resultSet.getString("last_name"));
        coach.setPhoneNumber(resultSet.getString("phone_number"));
        coach.setAddress(resultSet.getString("address"));
        coach.setExperience(resultSet.getInt("experience"));

        return coach;
    }
}
