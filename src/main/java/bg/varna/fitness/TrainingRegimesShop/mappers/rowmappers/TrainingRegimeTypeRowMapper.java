package bg.varna.fitness.TrainingRegimesShop.mappers.rowmappers;


import bg.varna.fitness.TrainingRegimesShop.domain.entities.TrainingRegimeType;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * @author Nikolay Uzunov
 */

@Component
public class TrainingRegimeTypeRowMapper implements RowMapper<TrainingRegimeType>
{
    @Override
    public TrainingRegimeType mapRow(ResultSet resultSet, int i) throws SQLException
    {
        TrainingRegimeType trainingRegimeType = new TrainingRegimeType();
        trainingRegimeType.setType(resultSet.getString("type"));

        return trainingRegimeType;
    }
}
