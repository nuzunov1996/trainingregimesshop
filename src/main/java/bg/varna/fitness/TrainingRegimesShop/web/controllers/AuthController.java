package bg.varna.fitness.TrainingRegimesShop.web.controllers;

import bg.varna.fitness.TrainingRegimesShop.domain.entities.User;
import bg.varna.fitness.TrainingRegimesShop.models.RegisterViewModel;
import bg.varna.fitness.TrainingRegimesShop.services.impl.UserServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("users")
public class AuthController
{
    private final UserServiceImpl userService;

    public AuthController(UserServiceImpl userService)
    {
        this.userService = userService;
    }

    @GetMapping("/login")
    public ModelAndView login(ModelAndView modelAndView)
    {
        modelAndView.setViewName("login");
        return modelAndView;
    }

    @GetMapping("/register")
    public ModelAndView register(ModelAndView modelAndView)
    {
        modelAndView.setViewName("register");
        return modelAndView;
    }

    @PostMapping("/register")
    public ModelAndView register(@ModelAttribute User user , ModelAndView modelAndView)
    {
        this.userService.register(user);
        modelAndView.setViewName("redirect:/home");
        return modelAndView;
    }
}
