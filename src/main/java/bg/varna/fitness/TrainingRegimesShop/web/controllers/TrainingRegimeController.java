package bg.varna.fitness.TrainingRegimesShop.web.controllers;


import bg.varna.fitness.TrainingRegimesShop.domain.entities.TrainingRegime;
import bg.varna.fitness.TrainingRegimesShop.filters.trainingregimefilters.TrainingRegimeDeleteFilter;
import bg.varna.fitness.TrainingRegimesShop.filters.trainingregimefilters.TrainingRegimePostFilter;
import bg.varna.fitness.TrainingRegimesShop.filters.trainingregimefilters.TrainingRegimePutFilter;
import bg.varna.fitness.TrainingRegimesShop.mappers.modelmapper.Mapper;
import bg.varna.fitness.TrainingRegimesShop.services.api.BaseService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Nikolay Uzunov
 */

@RestController
public class TrainingRegimeController
{
    private static Logger LOGGER = LogManager.getLogger(TrainingRegimeController.class);

    private BaseService service;

    private Mapper<TrainingRegime> mapper;

    public TrainingRegimeController(@Qualifier("trainingRegimeServiceImpl") BaseService service, Mapper mapper)
    {
        this.mapper = mapper;
        this.service = service;
    }

    /*
     * @GetMapping("regimes") public List<TrainingRegime> get(@Valid TrainingRegimeGetFilter filter) {
     * LOGGER.debug("!!!------->GET: " + filter); TrainingRegime trainingRegime = mapper.map(filter,
     * TrainingRegime.class); List<TrainingRegime> trainingRegimes = service.get(trainingRegime); return
     * trainingRegimes; }
     */


    @GetMapping("regimes")
    public ModelAndView get()
    {
        List<TrainingRegime> regimes = new ArrayList()
        {
            {
                add(new TrainingRegime.TrainingRegimeBuilder()
                        .setRegime("dfdfdsfsfdsfdsf")
                        .build());
                add(new TrainingRegime.TrainingRegimeBuilder()
                        .setRegime("dfdsfdsfsfddsfsf")
                        .build());
                add(new TrainingRegime.TrainingRegimeBuilder()
                        .setRegime("dfdfdsfsfddsfssf")
                        .build());
                add(new TrainingRegime.TrainingRegimeBuilder()
                        .setRegime("dfdfadssfsfdsfsf")
                        .build());
                add(new TrainingRegime.TrainingRegimeBuilder()
                        .setRegime("dfdfdsfsfdsfssf")
                        .build());
                add(new TrainingRegime.TrainingRegimeBuilder()
                        .setRegime("dfdfdsfsfdsfssf")
                        .build());
            }
        };
        ModelAndView model = new ModelAndView();
        model.setViewName("main");
        return model.addObject("regimes", regimes);
    }


    @PostMapping("regime")
    public void save(@RequestBody @Valid TrainingRegimePostFilter filter)
    {
        LOGGER.debug("!!!------->POST: " + filter);
        TrainingRegime trainingRegime = mapper.map(filter, TrainingRegime.class);
        service.save(trainingRegime);
    }


    @PutMapping("regime")
    public void update(@RequestBody @Valid TrainingRegimePutFilter filter)
    {
        LOGGER.debug("!!!------->PUT: " + filter);
        TrainingRegime trainingRegime = mapper.map(filter, TrainingRegime.class);
        service.update(trainingRegime);
    }


    @DeleteMapping("regime")
    public void delete(@RequestBody @Valid TrainingRegimeDeleteFilter filter)
    {
        LOGGER.debug("!!!------->DELETE: " + filter);
        TrainingRegime trainingRegime = mapper.map(filter, TrainingRegime.class);
        service.delete(trainingRegime);
    }
}
