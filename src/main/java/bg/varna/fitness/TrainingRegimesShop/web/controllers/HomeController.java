package bg.varna.fitness.TrainingRegimesShop.web.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.security.Principal;

@RestController
public class HomeController
{
    @GetMapping("/")
    public ModelAndView index(ModelAndView modelAndView)
    {
        modelAndView.setViewName("index");
        return modelAndView;
    }

    @GetMapping
    public Object home(HttpSession session, Principal principal, ModelAndView modelAndView)
    {
        modelAndView.addObject("user", principal.getName());
        modelAndView.setViewName("home");
        return modelAndView;
    }
}
