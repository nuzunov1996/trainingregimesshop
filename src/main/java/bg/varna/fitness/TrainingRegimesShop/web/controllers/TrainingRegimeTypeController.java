package bg.varna.fitness.TrainingRegimesShop.web.controllers;

import bg.varna.fitness.TrainingRegimesShop.domain.entities.TrainingRegimeType;
import bg.varna.fitness.TrainingRegimesShop.filters.trainingregimetypefilters.TrainingRegimeTypeDeleteFilter;
import bg.varna.fitness.TrainingRegimesShop.filters.trainingregimetypefilters.TrainingRegimeTypeGetFilter;
import bg.varna.fitness.TrainingRegimesShop.filters.trainingregimetypefilters.TrainingRegimeTypePostFilter;
import bg.varna.fitness.TrainingRegimesShop.filters.trainingregimetypefilters.TrainingRegimeTypePutFilter;
import bg.varna.fitness.TrainingRegimesShop.mappers.modelmapper.Mapper;
import bg.varna.fitness.TrainingRegimesShop.services.api.BaseService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Nikolay Uzunov
 */

@RestController
public class TrainingRegimeTypeController
{
    private static final Logger LOGGER = LogManager.getLogger(TrainingRegimeTypeController.class);
    private BaseService service;
    private Mapper<TrainingRegimeType> mapper;

    public TrainingRegimeTypeController(@Qualifier("trainingRegimeTypeServiceImpl") BaseService service, Mapper mapper)
    {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping("types")
    public void get(@RequestBody
                    @Valid TrainingRegimeTypeGetFilter filter)
    {
        LOGGER.debug("!!!------->DELETE: " + filter);
        TrainingRegimeType regimeType = mapper.map(filter, TrainingRegimeType.class);
        List<TrainingRegimeType> types = service.get(regimeType);
    }

    @PostMapping("type")
    public void save(@RequestBody
                     @Valid TrainingRegimeTypePostFilter filter)
    {
        LOGGER.debug("!!!------->POST: " + filter);
        TrainingRegimeType regimeType = mapper.map(filter, TrainingRegimeType.class);
        service.save(regimeType);
    }

    @PutMapping("type")
    public void update(@RequestBody
                       @Valid TrainingRegimeTypePutFilter filter)
    {
        LOGGER.debug("!!!------->PUT: " + filter);
        TrainingRegimeType regimeType = mapper.map(filter, TrainingRegimeType.class);
        service.update(regimeType);
    }

    @DeleteMapping("type")
    public void delete(@RequestBody
                       @Valid TrainingRegimeTypeDeleteFilter filter)
    {
        LOGGER.debug("!!!------->DELETE: " + filter);
        TrainingRegimeType regimeType = mapper.map(filter, TrainingRegimeType.class);
        service.delete(regimeType);
    }
}
