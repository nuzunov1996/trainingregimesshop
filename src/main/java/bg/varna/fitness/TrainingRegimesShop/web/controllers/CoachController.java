package bg.varna.fitness.TrainingRegimesShop.web.controllers;


import bg.varna.fitness.TrainingRegimesShop.domain.entities.Coach;
import bg.varna.fitness.TrainingRegimesShop.filters.coachfilters.CoachDeleteFilter;
import bg.varna.fitness.TrainingRegimesShop.filters.coachfilters.CoachPostFilter;
import bg.varna.fitness.TrainingRegimesShop.filters.coachfilters.CoachPutFilter;
import bg.varna.fitness.TrainingRegimesShop.mappers.modelmapper.Mapper;
import bg.varna.fitness.TrainingRegimesShop.services.api.BaseService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Nikolay Uzunov
 */

@RestController
public class CoachController
{
    private static final Logger LOGGER = LogManager.getLogger(CoachController.class);

    private Mapper<Coach> mapper;
    private BaseService service;

    public CoachController(@Qualifier("coachServiceImpl") BaseService service, Mapper mapper)
    {
        this.mapper = mapper;
        this.service = service;
    }
    
/*    @GetMapping("/")
    ModelAndView index(ModelAndView modelAndView)
    {
        Coach coach = new Coach();
        coach.setFirstName("nikolay");
        //modelAndView.addObject("coach", coach);
        modelAndView.setViewName("main");
        return modelAndView;
    }*/

    @GetMapping("coaches")
    public ModelAndView get()
    {
        ModelAndView model = new ModelAndView();
        model.setViewName("main");
        List<Coach> coaches = new ArrayList();
        Coach coach = new Coach.CoachBuilder()
                .setFirstName("niki")
                .build();
        Coach coach1 = new Coach.CoachBuilder()
                .setFirstName("niki1")
                .build();
        Coach coach2 = new Coach.CoachBuilder()
                .setFirstName("niki2")
                .build();
        Coach coach3 = new Coach.CoachBuilder()
                .setFirstName("niki3")
                .build();
        Coach coach4 = new Coach.CoachBuilder()
                .setFirstName("niki4")
                .build();
        Coach coach5 = new Coach.CoachBuilder()
                .setFirstName("niki5")
                .build();
       
        coaches.add(coach);
        coaches.add(coach1);
        coaches.add(coach2);
        coaches.add(coach3);
        coaches.add(coach4);
        coaches.add(coach5);
        model.addObject("coaches", coaches);
        return model;
    }
    /*
     * @GetMapping("coaches") public List<Coach> get(@Valid CoachGetFilter filter) { LOGGER.debug("!!!------->GET: " +
     * filter.toString()); Coach coach = mapper.map(filter, Coach.class); return coaches = service.get(coach); }
     */


    @PostMapping("coach")
    public void save(@RequestBody @Valid CoachPostFilter filter)
    {
        LOGGER.debug("!!!------->POST: " + filter);
        Coach coach = mapper.map(filter, Coach.class);
        service.save(coach);
    }


    @PutMapping("coach")
    public void update(@RequestBody @Valid CoachPutFilter filter)
    {
        LOGGER.debug("!!!------->PUT: " + filter);
        Coach coach = mapper.map(filter, Coach.class);
        service.update(coach);
    }


    @DeleteMapping("coach")
    public void delete(@RequestBody @Valid CoachDeleteFilter filter)
    {
        LOGGER.debug("!!!------->DELETE: " + filter);
        Coach coach = mapper.map(filter, Coach.class);
        service.delete(coach);
    }
}
