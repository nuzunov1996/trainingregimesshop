package bg.varna.fitness.TrainingRegimesShop.filters.trainingregimetypefilters;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

/**
 * @author Nikolay Uzunov
 */

public class TrainingRegimeTypePostFilter
{
    @JsonProperty
    private long id;

    @NotNull
    @JsonProperty
    private String type;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }
}
