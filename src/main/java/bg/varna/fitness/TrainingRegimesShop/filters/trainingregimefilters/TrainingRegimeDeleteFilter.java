package bg.varna.fitness.TrainingRegimesShop.filters.trainingregimefilters;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author Nikolay Uzunov
 */

public class TrainingRegimeDeleteFilter
{
    @NotNull
    @Min(1)
    @JsonProperty
    private long id;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }
}
