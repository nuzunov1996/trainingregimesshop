package bg.varna.fitness.TrainingRegimesShop.filters.trainingregimetypefilters;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Nikolay Uzunov
 */

public class TrainingRegimeTypeGetFilter
{
    @JsonProperty
    private String type;

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }
}
