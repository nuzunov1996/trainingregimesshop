package bg.varna.fitness.TrainingRegimesShop.filters.coachfilters;


import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * @author Nikolay Uzunov
 */

public class CoachPostFilter
{
    @JsonProperty
    private long id;

    @NotNull
    @Size(min = 2)
    @JsonProperty("first_name")
    private String firstName;

    @NotNull
    @Size(min = 2)
    @JsonProperty("last_name")
    private String lastName;

    @NotNull
    @Pattern(regexp = "08[789]\\d{7}")
    @JsonProperty("phone_number")
    private String phoneNumber;

    @NotNull
    @JsonProperty
    private String address;

    @NotNull
    @Min(0)
    @JsonProperty
    private int experience;

    public long getId()
    {
        return id;
    }


    public void setId(long id)
    {
        this.id = id;
    }


    public String getFirstName()
    {
        return firstName;
    }


    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }


    public String getLastName()
    {
        return lastName;
    }


    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }


    public String getPhoneNumber()
    {
        return phoneNumber;
    }


    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }


    public String getAddress()
    {
        return address;
    }


    public void setAddress(String address)
    {
        this.address = address;
    }


    public int getExperience()
    {
        return experience;
    }


    public void setExperience(int experience)
    {
        this.experience = experience;
    }
}
