package bg.varna.fitness.TrainingRegimesShop.filters.trainingregimetypefilters;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author Nikolay Uzunov
 */

public class TrainingRegimeTypeDeleteFilter
{
    @NotNull
    @Min(1)
    @JsonProperty
    private long id;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }
}
