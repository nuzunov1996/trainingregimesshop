package bg.varna.fitness.TrainingRegimesShop.filters.coachfilters;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Pattern;

/**
 * @author Nikolay Uzunov
 */

public class CoachGetFilter
{
    @JsonProperty
    private long id;

    @JsonProperty(value = "first_name")
    private String firstName;

    @JsonProperty(value = "last_name")
    private String lastName;

    @Pattern(regexp = "08[789]\\d{7}")
    @JsonProperty("phone_number")
    private String phoneNumber;

    @JsonProperty
    private String address;

    @JsonProperty
    private int experience;

    public void setId(long id)
    {
        this.id = id;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public void setExperience(int experience)
    {
        this.experience = experience;
    }

    public long getId()
    {
        return id;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public String getAddress()
    {
        return address;
    }

    public int getExperience()
    {
        return experience;
    }

    @Override
    public String toString()
    {
        return "CoachGetFilter [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", phoneNumber="
                + phoneNumber + ", address=" + address + ", experience=" + experience + "]";
    }
    
    
}
