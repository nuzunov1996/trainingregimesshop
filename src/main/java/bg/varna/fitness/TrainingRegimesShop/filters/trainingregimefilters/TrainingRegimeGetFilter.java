package bg.varna.fitness.TrainingRegimesShop.filters.trainingregimefilters;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

/**
 * @author Nikolay Uzunov
 */

public class TrainingRegimeGetFilter
{
    @JsonProperty
    private long id;

    @JsonProperty
    private String regime;

    @DecimalMin(value = "0.0")
    @JsonProperty
    private BigDecimal price;

    @Min(0)
    @Max(10)
    @JsonProperty
    private float rate;

    @Min(1)
    @JsonProperty("coach_id")
    private long coachId;

    @Min(1)
    @JsonProperty("training_regime_type_id")
    private long trainingRegimeTypeId;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getRegime()
    {
        return regime;
    }

    public void setRegime(String regime)
    {
        this.regime = regime;
    }

    public BigDecimal getPrice()
    {
        return price;
    }

    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }

    public float getRate()
    {
        return rate;
    }

    public void setRate(float rate)
    {
        this.rate = rate;
    }

    public long getCoachId()
    {
        return coachId;
    }

    public void setCoachId(long coachId)
    {
        this.coachId = coachId;
    }

    public long getTrainingRegimeTypeId()
    {
        return trainingRegimeTypeId;
    }

    public void setTrainingRegimeTypeId(long trainingRegimeTypeId)
    {
        this.trainingRegimeTypeId = trainingRegimeTypeId;
    }
}
