package bg.varna.fitness.TrainingRegimesShop.config;

import bg.varna.fitness.TrainingRegimesShop.domain.entities.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter
{
    private final DataSource dataSource;

    public WebSecurityConfiguration(DataSource dataSource)
    {
        this.dataSource = dataSource;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception
    {
        /*auth.jdbcAuthentication()
                .dataSource(dataSource)
                .withDefaultSchema()
                .withUser(new User("admin", "admin"))
                .usersByUsernameQuery("SELECT username, password, enabled " +
                        "FROM users " +
                        "WHERE username = ?")
                .authoritiesByUsernameQuery("SELECT username, authority " +
                        "FROM authorities " +
                        "WHERE username = ?");*/
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        /*http
                .authorizeRequests()
                .antMatchers("/", "/users/login", "/users/register").anonymous()
                .antMatchers("/", "/users/regimes").permitAll();*/
    }

    @Bean
    public PasswordEncoder passwordEncoder()
    {
        return new BCryptPasswordEncoder();
    }
}