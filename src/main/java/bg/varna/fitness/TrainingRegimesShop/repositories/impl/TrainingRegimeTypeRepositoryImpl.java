package bg.varna.fitness.TrainingRegimesShop.repositories.impl;

import bg.varna.fitness.TrainingRegimesShop.domain.entities.TrainingRegimeType;
import bg.varna.fitness.TrainingRegimesShop.mappers.rowmappers.TrainingRegimeTypeRowMapper;
import bg.varna.fitness.TrainingRegimesShop.repositories.api.TrainingRegimeTypeRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author Nikolay Uzunov
 */
@Repository
public class TrainingRegimeTypeRepositoryImpl implements TrainingRegimeTypeRepository
{
    private static final Logger LOGGER = LogManager.getLogger(TrainingRegimeTypeRepositoryImpl.class);

    private static final String TABLE_NAME = "training_regime_types";

    private static final String ID_FIELD = "id";

    private static final String TYPE_FIELD = "type";

    private static final String ACTIVE_FIELD = "active";

    private static final String GET_BY_ID_QUERY = "SELECT " + ID_FIELD + ", " + TYPE_FIELD + " FROM " + TABLE_NAME
            + " WHERE " + ID_FIELD + "= ?";

    private static final String GET_BY_TYPE_QUERY = "SELECT " + ID_FIELD + ", " + TYPE_FIELD + " FROM " + TABLE_NAME
            + " WHERE " + TYPE_FIELD + "= ?";

    private static final String GET_ALL_QUERY = "SELECT " + ID_FIELD + ", " + TYPE_FIELD + " FROM " + TABLE_NAME;

    private static final String INSERT_QUERY = "INSERT INTO " + TABLE_NAME + "(" + TYPE_FIELD + ", " + ACTIVE_FIELD
            + ") VALUES(?,?)";

    private static final String UPDATE_QUERY = "UPDATE " + TABLE_NAME + " SET " + TYPE_FIELD + "= ? WHERE " + ID_FIELD
            + "= ?";

    private static final String DELETE_QUERY = "DELETE FROM " + TABLE_NAME + " WHERE " + ID_FIELD + "= ?";
    
    private static final String DEACTIVATE_ROW = "UPDATE " + TABLE_NAME + " SET " + ACTIVE_FIELD + "= ? WHERE "
            + ID_FIELD + "= ?";

    private JdbcTemplate jdbcTemplate;

    private TrainingRegimeTypeRowMapper rowMapper;

    public TrainingRegimeTypeRepositoryImpl(JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
        this.rowMapper = new TrainingRegimeTypeRowMapper();
    }


    @Override
    public TrainingRegimeType getByType(String type)
    {
        LOGGER.debug("Operation getByType! TrainingRegimeType type: " + type);
        TrainingRegimeType trainingRegimeType = jdbcTemplate.queryForObject(GET_BY_TYPE_QUERY, rowMapper, type);
        return trainingRegimeType;
    }


    @Override
    public TrainingRegimeType getById(long id)
    {
        LOGGER.debug("Operation getById! TrainingRegimeType id: " + id);
        TrainingRegimeType trainingRegimeType = jdbcTemplate.queryForObject(GET_BY_ID_QUERY, rowMapper, id);
        return trainingRegimeType;
    }


    @Override
    public List<TrainingRegimeType> getAll()
    {
        List<TrainingRegimeType> trainingRegimeTypes = jdbcTemplate.query(GET_ALL_QUERY, rowMapper);
        return trainingRegimeTypes;
    }


    @Override
    public void save(TrainingRegimeType entity)
    {
        LOGGER.debug("Operation save! TrainingRegimeType entity: " + entity);
        jdbcTemplate.update(INSERT_QUERY, entity.getType(), true);
    }


    @Override
    public void delete(TrainingRegimeType entity)
    {
        LOGGER.debug("Operation delete! TrainingRegimeType entity: " + entity);
        jdbcTemplate.update(DELETE_QUERY, entity.getId());
    }


    @Override
    public void update(TrainingRegimeType entity)
    {
        LOGGER.debug("Operation update! TrainingRegimeType entity: " + entity);
        jdbcTemplate.update(UPDATE_QUERY, entity.getType(), entity.getId());
    }


    @Override
    public void deactivate(TrainingRegimeType entity)
    {
        LOGGER.debug("Operation deactivate! TrainingRegimeType entity: " + entity);
        jdbcTemplate.update(DEACTIVATE_ROW, false, entity.getId());
    }
}
