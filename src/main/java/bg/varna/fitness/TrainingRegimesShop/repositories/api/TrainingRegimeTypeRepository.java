package bg.varna.fitness.TrainingRegimesShop.repositories.api;

import bg.varna.fitness.TrainingRegimesShop.domain.entities.TrainingRegimeType;

/**
 * @author Nikolay Uzunov
 */
public interface TrainingRegimeTypeRepository extends BaseRepository<TrainingRegimeType>
{
    TrainingRegimeType getByType(String type);
}
