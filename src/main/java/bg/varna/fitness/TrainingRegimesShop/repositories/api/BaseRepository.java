package bg.varna.fitness.TrainingRegimesShop.repositories.api;

import java.util.List;

/**
 * @author Nikolay Uzunov
 */
public interface BaseRepository<T>
{
    T getById(long id);
    List<T> getAll();
    void save(T entity);
    void delete(T entity);
    void update(T entity);
    void deactivate(T entity);
}
