package bg.varna.fitness.TrainingRegimesShop.repositories.impl;

import bg.varna.fitness.TrainingRegimesShop.domain.entities.User;
import bg.varna.fitness.TrainingRegimesShop.repositories.api.UserRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class UserRepositoryImpl implements UserRepository
{
    private static final String TABLE_NAME = "public.users";

    private static final String ID_FIELD = "id";
    private static final String USERNAME_FIELD = "username";
    private static final String PASSWORD_FIELD = "password";

    private static final String GET_USER_BY_USERNAME = "SELECT " + ID_FIELD + ", "+ USERNAME_FIELD + ", " + PASSWORD_FIELD
            + " FROM " + TABLE_NAME + " WHERE " + USERNAME_FIELD + " = ?;";
    private static final String INSERT_USER = "INSERT INTO " + TABLE_NAME + " (" + USERNAME_FIELD + ", " + ", " + PASSWORD_FIELD + " VALUES(?, ?, ?);";

    private final JdbcTemplate jdbcTemplate;
    private static final RowMapper<User> rowMapper = (rs, rowNum) ->
    {
        User user = new User();
        user.setId(rs.getLong("id"));
        user.setUsername(rs.getString("username"));
        user.setUsername(rs.getString("password"));
        return user;
    };

    public UserRepositoryImpl(JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public User findByUsername(String username)
    {
        return jdbcTemplate.queryForObject(GET_USER_BY_USERNAME, rowMapper, username);
    }

    @Override
    public long insert(User user)
    {
        return jdbcTemplate.update(INSERT_USER, user.getUsername(), user.getPassword());
    }
}
