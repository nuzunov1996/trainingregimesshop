package bg.varna.fitness.TrainingRegimesShop.repositories.impl;

import bg.varna.fitness.TrainingRegimesShop.domain.entities.Coach;
import bg.varna.fitness.TrainingRegimesShop.mappers.rowmappers.CoachRowMapper;
import bg.varna.fitness.TrainingRegimesShop.repositories.api.CoachRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Nikolay Uzunov
 */

@Repository
public class CoachRepositoryImpl implements CoachRepository
{
    private static final Logger LOGGER = LogManager.getLogger(CoachRepositoryImpl.class);

    private static final String TABLE_NAME = "public.coaches";

    private static final String ID_FIELD = "id";

    private static final String FIRST_NAME_FIELD = "first_name";

    private static final String LAST_NAME_FIELD = "last_name";

    private static final String PHONE_NUMBER_FIELD = "phone_number";

    private static final String ADDRESS_FIELD = "address";

    private static final String EXPERIENCE_FIELD = "experience";

    private static final String ACTIVE_FIELD = "active";

    private static final String GET_BY_FIRST_NAME_QUERY = "SELECT " + ID_FIELD + ", " + FIRST_NAME_FIELD + ", "
            + LAST_NAME_FIELD +
            ", " + PHONE_NUMBER_FIELD + ", " + ADDRESS_FIELD + ", " + EXPERIENCE_FIELD + " FROM " + TABLE_NAME
            + " WHERE " + FIRST_NAME_FIELD + " = ?";

    private static final String GET_BY_LAST_NAME_QUERY = "SELECT " + ID_FIELD + ", " + FIRST_NAME_FIELD + ", "
            + LAST_NAME_FIELD + ", " + PHONE_NUMBER_FIELD + ", " + ADDRESS_FIELD + ", " + EXPERIENCE_FIELD + " FROM "
            + TABLE_NAME + " WHERE " + LAST_NAME_FIELD + " = ?";

    private static final String GET_BY_FULLNAME_QUERY = "SELECT " + ID_FIELD + ", " + FIRST_NAME_FIELD + ", "
            + LAST_NAME_FIELD + ", " + PHONE_NUMBER_FIELD + ", " + ADDRESS_FIELD + ", " + EXPERIENCE_FIELD + " FROM "
            + TABLE_NAME + " WHERE " + FIRST_NAME_FIELD + " = ? AND " + LAST_NAME_FIELD + " = ?";

    private static final String GET_BY_ID_QUERY = "SELECT " + ID_FIELD + ", " + FIRST_NAME_FIELD + ", "
            + LAST_NAME_FIELD + ", " + PHONE_NUMBER_FIELD + ", " + ADDRESS_FIELD + ", " + EXPERIENCE_FIELD + " FROM "
            + TABLE_NAME + " WHERE " + ID_FIELD + " = ?";

    private static final String GET_ALL_QUERY = "SELECT " + ID_FIELD + ", " + FIRST_NAME_FIELD + ", " + LAST_NAME_FIELD
            + ", " + PHONE_NUMBER_FIELD + ", " + ADDRESS_FIELD + ", " + EXPERIENCE_FIELD + " FROM " + TABLE_NAME;

    private static final String INSERT_QUERY = "INSERT INTO " + TABLE_NAME + "(" + FIRST_NAME_FIELD + ", "
            + LAST_NAME_FIELD + ", " + PHONE_NUMBER_FIELD + ", " + ADDRESS_FIELD + ", " + EXPERIENCE_FIELD + ", "
            + ACTIVE_FIELD + ") VALUES(?,?,?,?,?,?)";

    private static final String DELETE_QUERY = "DELETE FROM " + TABLE_NAME + " WHERE " + ID_FIELD + "= ?";

    private static final String UPDATE_QUERY = "UPDATE " + TABLE_NAME + " SET " + FIRST_NAME_FIELD + "= ?, "
            + LAST_NAME_FIELD + "= ?, " + PHONE_NUMBER_FIELD + "= ?, " + ADDRESS_FIELD + "= ?, " + EXPERIENCE_FIELD
            + "= ? WHERE " + ID_FIELD + "= ?";

    private static final String DEACTIVATE_ROW = "UPDATE " + TABLE_NAME + " SET " + ACTIVE_FIELD + "= ? WHERE "
            + ID_FIELD + "= ?";

    private List<Coach> coaches;

    private JdbcTemplate jdbcTemplate;

    private CoachRowMapper rowMapper;

    public CoachRepositoryImpl(JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
        this.rowMapper = new CoachRowMapper();
        this.coaches = new ArrayList<>();
    }


    @Override
    public List<Coach> getByName(String name)
    {
        LOGGER.debug("Operation getByName! Coach name: " + name);
        coaches = jdbcTemplate.query(GET_BY_FIRST_NAME_QUERY, rowMapper, name);
        return coaches;
    }


    @Override
    public List<Coach> getByLastName(String lastName)
    {
        LOGGER.debug("Operation getByLastName! Coach last name: " + lastName);
        coaches = jdbcTemplate.query(GET_BY_LAST_NAME_QUERY, rowMapper, lastName);
        return coaches;
    }


    @Override
    public List<Coach> getByFullName(String firstName, String lastName)
    {
        LOGGER.debug("Operation getByFullName! Coach fist name: " + firstName + ", last name: " + lastName);
        coaches = jdbcTemplate.query(GET_BY_FULLNAME_QUERY, rowMapper, firstName, lastName);
        return coaches;
    }


    @Override
    public Coach getById(long id)
    {
        LOGGER.debug("Operation getById! Coach name: " + id);
        Coach coach = jdbcTemplate.queryForObject(GET_BY_ID_QUERY, rowMapper, id);
        return coach;
    }


    @Override
    public List<Coach> getAll()
    {
        coaches = jdbcTemplate.query(GET_ALL_QUERY, rowMapper);
        return coaches;
    }


    @Override
    public void save(Coach entity)
    {
        LOGGER.debug("Operation save! Coach entity: " + entity);
        jdbcTemplate.update(INSERT_QUERY, entity.getFirstName(), entity.getLastName(), entity.getPhoneNumber(),
                entity.getAddress(), entity.getExperience(), entity.getId(), true);
    }


    @Override
    public void delete(Coach entity)
    {
        LOGGER.debug("Operation delete! Coach entity: " + entity);
        jdbcTemplate.update(DELETE_QUERY, entity.getId());
    }


    @Override
    public void update(Coach entity)
    {
        LOGGER.debug("Operation update! Coach entity: " + entity);
        jdbcTemplate.update(UPDATE_QUERY, entity.getFirstName(), entity.getLastName(), entity.getPhoneNumber(),
                entity.getAddress(), entity.getExperience(), entity.getId());
    }


    @Override
    public void deactivate(Coach entity)
    {
        LOGGER.debug("Operation deactivate! Coach entity: " + entity);
        jdbcTemplate.update(DEACTIVATE_ROW, false, entity.getId());
    }
}
