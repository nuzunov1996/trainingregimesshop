package bg.varna.fitness.TrainingRegimesShop.repositories.api;

import bg.varna.fitness.TrainingRegimesShop.domain.entities.Coach;

import java.util.List;

/**
 * @author Nikolay Uzunov
 */
public interface CoachRepository extends BaseRepository<Coach>
{
    List<Coach> getByName(String name);
    List<Coach> getByLastName(String lastName);
    List<Coach> getByFullName(String firstName, String lastName);
}
