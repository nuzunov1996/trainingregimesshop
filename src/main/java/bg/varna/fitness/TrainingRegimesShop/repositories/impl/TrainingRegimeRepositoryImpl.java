package bg.varna.fitness.TrainingRegimesShop.repositories.impl;

import bg.varna.fitness.TrainingRegimesShop.domain.entities.TrainingRegime;
import bg.varna.fitness.TrainingRegimesShop.mappers.rowmappers.TrainingRegimeRowMapper;
import bg.varna.fitness.TrainingRegimesShop.repositories.api.TrainingRegimeRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Nikolay Uzunov
 */

@Repository
public class TrainingRegimeRepositoryImpl implements TrainingRegimeRepository
{
    private static final Logger LOGGER = LogManager.getLogger(TrainingRegimeRepositoryImpl.class);

    private static final String TABLE_NAME = "public.training_regimes";

    private static final String ID_FIELD = "id";

    private static final String REGIME_FIELD = "regime";

    private static final String PRICE_FIELD = "price";

    private static final String RATE_FIELD = "rate";

    private static final String TRAINING_REGIME_TYPE_ID_FIELD = "training_regime_type_id";

    private static final String COACH_ID_FIELD = "coach_id";

    private static final String ACTIVE_FIELD = "active";

    private static final String GET_BY_RATE_QUERY = "SELECT " + ID_FIELD + ", " + REGIME_FIELD + ", " + PRICE_FIELD
            + ", " + RATE_FIELD + ", " + TRAINING_REGIME_TYPE_ID_FIELD + ", " + COACH_ID_FIELD + " FROM " + TABLE_NAME
            + " WHERE " + RATE_FIELD + "= ?";

    private static final String GET_BY_COACH_ID_QUERY = "SELECT " + ID_FIELD + ", " + REGIME_FIELD + ", " + PRICE_FIELD
            + ", " + RATE_FIELD + ", " + TRAINING_REGIME_TYPE_ID_FIELD + ", " + COACH_ID_FIELD + " FROM " + TABLE_NAME
            + " WHERE " + COACH_ID_FIELD + "= ?";

    private static final String GET_BY_TRAINING_REGIME_TYPE_ID_QUERY = "SELECT " + ID_FIELD + ", " + REGIME_FIELD + ", "
            + PRICE_FIELD + ", " + RATE_FIELD + ", " + TRAINING_REGIME_TYPE_ID_FIELD + ", " + COACH_ID_FIELD + " FROM "
            + TABLE_NAME + " WHERE " + TRAINING_REGIME_TYPE_ID_FIELD + "= ?";

    private static final String GET_BY_ID_QUERY = "SELECT " + ID_FIELD + ", " + REGIME_FIELD + ", " + PRICE_FIELD + ", "
            + RATE_FIELD + ", " + TRAINING_REGIME_TYPE_ID_FIELD + ", " + COACH_ID_FIELD + " FROM " + TABLE_NAME
            + " WHERE " + ID_FIELD + "= ?";

    private static final String GET_ALL_QUERY = "SELECT " + ID_FIELD + ", " + REGIME_FIELD + ", " + PRICE_FIELD + ", "
            + RATE_FIELD + ", " + TRAINING_REGIME_TYPE_ID_FIELD + ", " + COACH_ID_FIELD + " FROM " + TABLE_NAME;

    private static final String INSERT_QUERY = "INSERT INTO " + TABLE_NAME + "(" + REGIME_FIELD + ", " + PRICE_FIELD
            + ", " + RATE_FIELD + ", " + TRAINING_REGIME_TYPE_ID_FIELD + ", " + COACH_ID_FIELD + ") VALUES(?,?,?,?,?)";

    private static final String UPDATE_QUERY = "UPDATE " + TABLE_NAME + " SET " + REGIME_FIELD + "= ?, " + PRICE_FIELD
            + "= ?, " + RATE_FIELD + "= ?, " + TRAINING_REGIME_TYPE_ID_FIELD + "= ?, " + COACH_ID_FIELD + "= ? WHERE "
            + ID_FIELD + "= ?";

    private static final String DEACTIVATE_ROW_QUERY = "UPDATE " + TABLE_NAME + " SET " + ACTIVE_FIELD + "= ?"
            + " WHERE " + ID_FIELD + "= ?";

    private static final String DELETE_QUERY = "DELETE FROM " + TABLE_NAME + " WHERE " + ID_FIELD + "= ?";

    private List<TrainingRegime> trainingRegimes;

    private JdbcTemplate jdbcTemplate;

    private TrainingRegimeRowMapper rowMapper;

    public TrainingRegimeRepositoryImpl(JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
        this.rowMapper = new TrainingRegimeRowMapper();
        this.trainingRegimes = new ArrayList<>();
    }


    @Override
    public List<TrainingRegime> getByRate(float rate)
    {
        LOGGER.debug("Operation getByRate! TrainingRegime rate: " + rate);
        trainingRegimes = jdbcTemplate.query(GET_BY_RATE_QUERY, rowMapper, rate);
        return trainingRegimes;
    }


    @Override
    public List<TrainingRegime> getByCoachId(long coachId)
    {
        LOGGER.debug("Operation getByCoachId! TrainingRegime coach id: " + coachId);
        trainingRegimes = jdbcTemplate.query(GET_BY_COACH_ID_QUERY, rowMapper, coachId);
        return trainingRegimes;
    }


    @Override
    public List<TrainingRegime> getByTrainingRegimeTypeId(long trainingRegimeTypeId)
    {
        LOGGER.debug("Operation getByTrainingRegimeTypeId! TrainingRegime training regime type id: "
                + trainingRegimeTypeId);
        trainingRegimes = jdbcTemplate.query(GET_BY_TRAINING_REGIME_TYPE_ID_QUERY, rowMapper,
                trainingRegimeTypeId);
        return trainingRegimes;
    }


    @Override
    public TrainingRegime getById(long id)
    {
        LOGGER.debug("Operation getById! TrainingRegime id: " + id);
        TrainingRegime trainingRegime = jdbcTemplate.queryForObject(GET_BY_ID_QUERY, rowMapper, id);
        return trainingRegime;
    }


    @Override
    public List<TrainingRegime> getAll()
    {
        trainingRegimes = jdbcTemplate.query(GET_ALL_QUERY, rowMapper);
        return trainingRegimes;
    }


    @Override
    public void save(TrainingRegime entity)
    {
        LOGGER.debug("Operation save! TrainingRegime entity: " + entity);
        jdbcTemplate.update(INSERT_QUERY, entity.getRegime(), entity.getPrice(), entity.getRate(),
                entity.getTrainingRegimeTypeId(), entity.getCoachId());
    }


    @Override
    public void delete(TrainingRegime entity)
    {
        LOGGER.debug("Operation delete! TrainingRegime entity: " + entity);
        jdbcTemplate.update(DELETE_QUERY, entity.getId());
    }


    @Override
    public void update(TrainingRegime entity)
    {
        LOGGER.debug("Operation update! TrainingRegime entity: " + entity);
        jdbcTemplate.update(UPDATE_QUERY, entity.getRegime(), entity.getPrice(), entity.getRate(),
                entity.getTrainingRegimeTypeId(), entity.getCoachId());
    }


    @Override
    public void deactivate(TrainingRegime entity)
    {
        LOGGER.debug("Operation deactivate! TrainingRegime entity: " + entity);
        jdbcTemplate.update(DEACTIVATE_ROW_QUERY, false,
                entity.getTrainingRegimeTypeId());
    }
}
