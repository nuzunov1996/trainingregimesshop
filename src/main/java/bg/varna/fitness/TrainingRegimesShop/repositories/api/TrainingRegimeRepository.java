package bg.varna.fitness.TrainingRegimesShop.repositories.api;

import bg.varna.fitness.TrainingRegimesShop.domain.entities.TrainingRegime;

import java.util.List;

/**
 * @author Nikolay Uzunov
 */
public interface TrainingRegimeRepository extends BaseRepository<TrainingRegime>
{
    List<TrainingRegime> getByRate(float rate);
    List<TrainingRegime> getByCoachId(long coachId);
    List<TrainingRegime> getByTrainingRegimeTypeId(long trainingRegimeTypeId);
}
