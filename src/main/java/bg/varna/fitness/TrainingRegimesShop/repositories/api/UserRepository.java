package bg.varna.fitness.TrainingRegimesShop.repositories.api;

import bg.varna.fitness.TrainingRegimesShop.domain.entities.User;

public interface UserRepository
{
    User findByUsername(String username);

    long insert(User user);
}
